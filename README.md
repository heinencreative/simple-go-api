# Simple API #

Practice exercise building a RESTful API using Go. This exercise builds upon the example below by including the missing handlers for updating and destroying records.

## Requirements ##

• [Go](https://golang.org/doc/install)

## Setup ##

1. Configure [$GOPATH](https://golang.org/doc/code.html#GOPATH)
2. `cd $GOPATH`
3. `go get bitbucket.org/heinencreative/simple-go-api`
4. `simple-go-api`

## Resource ##

[http://thenewstack.io/make-a-restful-json-api-go/](http://thenewstack.io/make-a-restful-json-api-go/)
